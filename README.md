De-ZUCCCing your friend's birthdays have never been easier
This script is what came after a long decision between a) writing it 2) getting the birthdays from my closest ones from facebook by hand
Happens that I found the site https://www.facebook.com/events/birthdays/ which has a list with circles, each one representing a ""friend"". The funny part comes that when you hover those balls you get: Friend's Name (Birth/Day) so you can run some badass regexes to extract those.
Yea, you could do that, or you can use my script that pretty much does that.

First enter https://www.facebook.com/events/birthdays/ and scroll all the way down (I assume you won't get the whole list if you don't) and then save the page under a fancy name like `bdays.html`.
Run the script, passing the html as a parameter like this:
`./fb_bday_extract bdays.html`
And voilá, it will return the list of all birthdays, sorted by date. 
